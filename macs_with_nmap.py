#!/usr/bin/env python
import nmap #import python-nmap
nm = nmap.PortScanner()
cidr2='192.168.0.0/24'

a=nm.scan(arguments='-sS -p T:22', hosts=cidr2)

for k, v in a['scan'].items():
    print(v['addresses']['mac'])
